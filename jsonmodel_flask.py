from keras.models import Sequential
from keras.layers import Dense
from sklearn.preprocessing import LabelEncoder, OneHotEncoder
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
from sklearn.preprocessing import Imputer
from sklearn.externals import joblib


from flask import Flask, jsonify, request

import pandas as pd
import numpy as np

import dill as pickle
import json


# fix random seed for reproducibility
np.random.seed(7)

# fix random seed 
np.random.seed(7)
#global b
'''
def ANN(json_data):
    # reading the JSON data using json.load()
#     with open('jDATASET.json') as data_file:    
 #       df = pd.read_json(data_file, orient='records')
    
    
    df = pd.read_json(json_data, orient='records')
    print(df)

   

    X=df.loc[:,['VILLAGE NAME','RAINFALL AMOUNT','DAM CONTRIBUTION','ELEVATION']].values
    Y=df.loc[:,'OUTCOME'].values
    #print(X)
    #print(Y)


    # handle missing data
    imputer=Imputer(missing_values= 'NaN', strategy = 'mean', axis = 0)
    imputer = imputer.fit(X[:, 2:4])
    X[:, 2:4] = imputer.transform(X[:, 2:4])

    # Level Encoding for strings
    
    lE_village=LabelEncoder()
    X[:, 0] = lE_village.fit_transform(X[:, 0])
    #OHE_year = OneHotEncoder(categorical_features = [0])

    OHE_village = OneHotEncoder(categorical_features = [0])
    #X = OHE_year.fit_transform(X).toarray()
    X = OHE_village.fit_transform(X).toarray()


    # preparing the training and the test data sets
    X_train,X_test,Y_train,Y_test = train_test_split(X,Y,test_size = 0.2, random_state=0)

    # Scaling training and the test datasets to be standardized.
    sc_X = StandardScaler()
    X_train= sc_X.fit_transform(X_train)
    X_test= sc_X.transform(X_test)



    # create model
    model = Sequential()
    model.add(Dense(output_dim= 7, input_dim=13,init = 'uniform', activation='relu'))
    model.add(Dense(output_dim= 7, init = 'uniform', activation='relu'))
    model.add(Dense(1,init = 'uniform', activation='sigmoid'))

    # Compile model

    model.compile(loss='binary_crossentropy', optimizer='adam', metrics=['accuracy'])

    # Fit the model
    model.fit(X_train, Y_train, epochs=250, batch_size=10, verbose=2)

    # evaluate the model
    scores = model.evaluate(X_test, Y_test)
    print("\n%s: %.2f%%" % (model.metrics_names[1], scores[1]*100))

    # calculate predictions
    predictions = model.predict(X_test)

    # round predictions
    rounded = [x[0] for x in predictions]
    #print(rounded)

    #print(predictions)


    ## JSON PARSER
    Z1=df.loc[:,['YEAR']].values.tolist()
    Z2=df.loc[:,['VILLAGE NAME']].values.tolist()
    Out_list=list(zip(Z1,Z2,rounded))

    b = []
    for row in Out_list: 
        a = {
        "Date":row[0],
        "Village":row[1],
        "Output":row[2]
        }
        b.append(a)
    return(b)

'''
app = Flask(__name__)
@app.route("/predict", methods=["POST"])
def predict():
    # initialize the data dictionary that will be returned from the
    # view
    data = {"success": False}
    # reading the JSON data using json.load()
    ''' with open('jDATASET.json') as data_file:    
        df = pd.read_json(data_file, orient='records')'''
    
    
    df = pd.read_json(request.data, orient='records')
    print(df)

   

    X=df.loc[:,['VILLAGE NAME','RAINFALL AMOUNT','DAM CONTRIBUTION','ELEVATION']].values
    Y=df.loc[:,'OUTCOME'].values
    #print(X)
    #print(Y)
    total_len=len(Y)
    #print(total_len)
    empty_len=0
    #print(Y)
    for i in np.nditer(Y):
    #print(i)
        if int(i)==-1:
            empty_len=empty_len+1
    testsize=(empty_len)/total_len

    # handle missing data
    imputer=Imputer(missing_values= 'NaN', strategy = 'mean', axis = 0)
    imputer = imputer.fit(X[:, 2:4])
    X[:, 2:4] = imputer.transform(X[:, 2:4])

    # Level Encoding for strings
    
    lE_village=LabelEncoder()
    X[:, 0] = lE_village.fit_transform(X[:, 0])
    #OHE_year = OneHotEncoder(categorical_features = [0])

    OHE_village = OneHotEncoder(categorical_features = [0])
    #X = OHE_year.fit_transform(X).toarray()
    X = OHE_village.fit_transform(X).toarray()


    # preparing the training and the test data sets
    X_train,X_test,Y_train,Y_test = train_test_split(X,Y,test_size = testsize, random_state=0)

    # Scaling training and the test datasets to be standardized.
    sc_X = StandardScaler()
    X_train= sc_X.fit_transform(X_train)
    X_test= sc_X.transform(X_test)



    # create model
    model = Sequential()
    model.add(Dense(output_dim= 7, input_dim=13,init = 'uniform', activation='relu'))
    model.add(Dense(output_dim= 7, init = 'uniform', activation='relu'))
    model.add(Dense(1,init = 'uniform', activation='sigmoid'))

    # Compile model
    model.compile(loss='binary_crossentropy', optimizer='adam', metrics=['accuracy'])

    # Fit the model
    model.fit(X_train, Y_train, epochs=53, batch_size=10, verbose=2)

    # evaluate the model
    #scores = model.evaluate(X_test, Y_test)
    #print("\n%s: %.2f%%" % (model.metrics_names[1], scores[1]*100))

    # calculate predictions
    predictions = model.predict(X_test)

    # round predictions
    rounded = [x[0] for x in predictions]
    #print(rounded)

    #print(predictions)


    ## JSON PARSER
    Z1=df.loc[:,['YEAR']].values.tolist()
    Z2=df.loc[:,['VILLAGE NAME']].values.tolist()
    Out_list=list(zip(Z1,Z2,rounded))

    b = []
    for row in Out_list: 
        a = {
        "Date":row[0],
        "Village":row[1],
        "Output":row[2]
        }
        b.append(a)
    return(str(b))
 
if __name__ == "__main__":
	print(("* Loading Keras model and Flask starting server..."
		"please wait until server has fully started"))
	
	app.run()