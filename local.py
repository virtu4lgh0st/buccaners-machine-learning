# -*- coding: utf-8 -*-
"""
Created on Sun Mar 25 11:26:37 2018

@author: gh0st
"""

import json
import requests
import pandas as pd
"""Setting the headers to send and accept json responses
"""
header = {'Content-Type': 'application/json', \
                  'Accept': 'application/json'}

"""Reading test batch
"""

#df = pd.read_csv('DATASET.csv', encoding="utf-8-sig")
#df = df.head()

with open('jDATASET.json') as data_file:    
    df = pd.read_json(data_file, orient='records')
    
    """Converting Pandas Dataframe to json
    """
data = df.to_json(orient='records')
#data = data_file
#print(data)
"""POST <url>/predict
"""
resp = requests.post("http://0.0.0.0:8000/predict", \
                    data = json.dumps(data),\
                    headers= header)
"""The final response we get is as follows:
"""
resp.json()
