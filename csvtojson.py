# -*- coding: utf-8 -*-
"""
Created on Sat Mar 24 16:55:35 2018

@author: gh0st
"""

import csv
import json

csvfile = open('DATASET.csv', 'r')
jsonfile = open('jDATASET.json', 'w')

fieldnames = ("YEAR","VILLAGE NAME","RAINFALL AMOUNT","DAM CONTRIBUTION","ELEVATION","OUTCOME")
reader = csv.DictReader( csvfile, fieldnames)
for row in reader:
    json.dump(row, jsonfile)
    jsonfile.write('\n')